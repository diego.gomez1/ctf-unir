PS1='\[\e[0;38;5;40m\]\u\[\e[0m\]@\[\e[0m\]\H\[\e[0m\]:\[\e[0m\]\w\[\e[0;38;5;190m\]$\[\e[0m\] '

cat << EOF

Cada uno que accedéis con este usuario tenéis una prueba
diferente. Se acaba de generar un sistema de archivos exclusivo
para tí. Para que encuentres el flag.

EOF

tmp_dir=$(mktemp -d -t ci-XXXXXXXXXX)
cd $tmp_dir
gofiles $tmp_dir
