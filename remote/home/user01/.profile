PS1='\[\e[0;38;5;40m\]\u\[\e[0m\]@\[\e[0m\]\H\[\e[0m\]:\[\e[0m\]\w\[\e[0;38;5;190m\]$\[\e[0m\] '

cat << EOF
Te acabas de conectar al entorno de pruebas de TIC I 🚀

Es posible que no puedas escribir la tecla "/" un truco
es que uses la tecla "ç" y debería aparecerte.

Recuerda que puedes usar el comando "man" para ver información
de los diferentes comandos.

Be Good! 👼

La primera información que debes conocer es las Flag del sistema
como has conseguido entrar te daré una pista muy evidente:
La primera flag que has descrubierto es ideo{este_es_el_primer_flag}
Ves a IDEO TIC CTF e introduce ese Flag. Si no lo ves por tí mismo
es que has dejado de leer antes de lo debías.
Agudiza tus sentidos y atención que estos retos los requieren.
Cuando hayas terminado escribe exit para salir.

EOF

alias find="echo 'Comando no disponible aún'"
alias grep="echo 'Comando no disponible aún'"
alias strings="echo 'Comando no disponible aún'"
alias man="echo 'Por ahora mira en tus apuntes, tienes lo que necesitas.'"
