function main() {
  printMessage(true);
}

function printMessage(reallyPrint) {
  if (reallyPrint != true) {
    return;
  }

  let words = [
    "6f 72 6d 61 63 69 f3 6e 2e",
    "70 61 72 61 20 65 6e 63 6f 6e 74 72 61 72 20 6c 61 20 69 6e 66 ",
    "61 72 e1 20 6c 6f 20 71 75 65 20 6e 65 63 65 73 69 74 61 73 20 ",
    "76 6f 73 20 6f 63 75 6c 74 6f 73 2c 20 65 73 6f 20 74 65 20 64 ",
    "4d 69 72 61 20 63 6f 6e 20 6c 73 20 6c 6f 73 20 61 72 63 68 69 ",
  ];
  console.log(words.reverse().join(""));
}

main();
