PS1='\[\e[0;38;5;40m\]\u\[\e[0m\]@\[\e[0m\]\H\[\e[0m\]:\[\e[0m\]\w\[\e[0;38;5;190m\]$\[\e[0m\] '

cat << EOF

A veces la respuesta está en lo más evidente, otras
se esconde de forma que no haya forma de encontrarlo.

El escondite menos pensado es el más visible.

EOF

alias find="echo 'Comando no disponible aún'"
alias grep="echo 'Comando no disponible aún'"