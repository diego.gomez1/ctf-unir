PS1='\[\e[0;38;5;40m\]\u\[\e[0m\]@\[\e[0m\]\H\[\e[0m\]:\[\e[0m\]\w\[\e[0;38;5;190m\]$\[\e[0m\] '

cat << EOF

El dominio de muchos comandos pasa por leer con detenimiento
la documentación. Uno de estos comandos es ls, que quizá lo
uses con mucha frecuencia.

Si aprendes bien ls seguramente resuelvas la siguiente pista.

EOF

alias strings="echo 'Comando no disponible aún'"
alias find="echo 'Comando no disponible aún'"
alias grep="echo 'Comando no disponible aún'"