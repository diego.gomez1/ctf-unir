PS1='\[\e[0;38;5;40m\]\u\[\e[0m\]@\[\e[0m\]\H\[\e[0m\]:\[\e[0m\]\w\[\e[0;38;5;190m\]$\[\e[0m\] '

cat << EOF

Alguien diría que los directorios sirven para organizar la información
pero yo no los necesito. Aquí tengo todo lo que necesito para trabajar.

EOF

alias find="echo 'Comando no disponible aún'"
alias grep="echo 'Comando no disponible aún'"
alias strings="echo 'Comando no disponible aún'"
alias man="echo 'Por ahora mira en tus apuntes, tienes lo que necesitas.'"