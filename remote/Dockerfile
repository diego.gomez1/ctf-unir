FROM golang:1.8 as gobuild

WORKDIR /go/src/tools
COPY containers/ssh/tools/goflag /go/src/tools/goflag
COPY containers/ssh/tools/gofiles /go/src/tools/gofiles

RUN cd /go/src/tools/goflag && go get . && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build
RUN cd /go/src/tools/gofiles && go get . && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build


FROM alpine:3.13.6
COPY containers/ssh/rootfs/entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]
EXPOSE 22

RUN apk add libc6-compat libcap

RUN apk add --no-cache openssh \
  && sed -i s/#PermitRootLogin.*/PermitRootLogin\ yes/ /etc/ssh/sshd_config \
  && echo "root:root" | chpasswd

RUN chmod +x /entrypoint.sh

RUN apk add mandoc man-pages

RUN adduser -D -h /home/user01 -s /bin/sh user01 && \
    ( echo "user01:linux" | chpasswd )
COPY containers/ssh/home/user01 /home/user01
RUN chown root:user01 /home/user01

RUN adduser -D -h /home/user02 -s /bin/sh user02 && \
    ( echo "user02:LinusTorvalds" | chpasswd )
COPY containers/ssh/home/user02 /home/user02
RUN chown root:user02 /home/user02

RUN adduser -D -h /home/user03 -s /bin/sh user03 && \
    ( echo "user03:Kernel" | chpasswd )
COPY containers/ssh/home/user03 /home/user03
RUN chown root:user03 /home/user03

RUN adduser -D -h /home/user04 -s /bin/sh user04 && \
    ( echo "user04:GNULinux" | chpasswd )
COPY containers/ssh/home/user04 /home/user04
COPY containers/ssh/files/ls.1p.gz /usr/share/man/man1
RUN chown root:user04 /home/user04

RUN adduser -D -h /home/user05 -s /bin/sh user05 && \
    ( echo "user05:Debian" | chpasswd )
COPY containers/ssh/home/user05 /home/user05
RUN chown root:user05 /home/user05

RUN adduser -D -h /home/user06 -s /bin/sh user06 && \
    ( echo "user06:0100110101" | chpasswd )
COPY containers/ssh/home/user06 /home/user06
RUN chown root:user06 /home/user06

RUN adduser -D -h /home/user07 -s /bin/sh user07 && \
    ( echo "user07:lots&lotsofFiles" | chpasswd )
COPY containers/ssh/home/user07 /home/user07
RUN chown root:user07 /home/user07

RUN adduser -D -h /home/user08 -s /bin/sh user08 && \
    ( echo "user08:computersCanSearch" | chpasswd )
COPY containers/ssh/home/user08 /home/user08
RUN chown root:user08 /home/user08

RUN adduser -D -h /home/user09 -s /bin/sh user09 && \
    ( echo "user09:notOnlyText!" | chpasswd )
COPY containers/ssh/home/user09 /home/user09
RUN chown root:user09 /home/user09

COPY --from=0 /go/src/tools/goflag/goflag /usr/local/bin/.
RUN chmod +x /usr/local/bin/goflag && chmod u+s /usr/local/bin/goflag
COPY --from=0 /go/src/tools/gofiles/gofiles /usr/local/bin/.
RUN chmod +x /usr/local/bin/gofiles
RUN chmod 750 /home/user*

COPY containers/ssh/users/welcome /etc/motd
