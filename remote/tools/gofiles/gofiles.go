package main

import (
	crand "crypto/rand"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"time"

	randomfiles "github.com/jbenet/go-random-files"
)

var opts randomfiles.Options
var quiet bool
var alphabet string
var paths []string
var cryptorand bool

type CreateRandomTreeParameters struct {
	basedir     string
	nFiles      int
	nFolders    int
	repeat      int
	maxDepth    int
	sigmaFolder int
	sigmaFiles  int
}

func getFolderInPath(path string) ([]string, error) {
	var folders []string
	err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			folders = append(folders, path)
		}
		return nil
	})
	return folders, err
}

func createRandomFlagFile(rootPath string) error {
	folders1, err := getFolderInPath(rootPath)

	if err == nil {
		folderIndex := rand.Intn(len(folders1))
		folders2, err := getFolderInPath(folders1[folderIndex])

		if err == nil {
			folderIndex := rand.Intn(len(folders2))
			folders3, err := getFolderInPath(folders2[folderIndex])

			if err == nil {
				folderIndex := rand.Intn(len(folders3))
				customId := fmt.Sprintf("%06d", rand.Intn(10000))
				fileName := folders3[folderIndex] + "/ideo{custom_folders_" + customId + "_flag}"
				d1 := []byte("Good\nwork!\n")
				err := ioutil.WriteFile(fileName, d1, 0644)
				return err
			} else {
				return err
			}
		} else {
			return err
		}
	} else {
		return err
	}

	return nil
}

func main() {

	if len(os.Args) == 1 {
		fmt.Println("A folder name ins required")
		fmt.Println("Usage:", os.Args[0], "FOLDER")
		return
	} else if len(os.Args) == 2 {
		folder := os.Args[1]
		fmt.Println("Preparing structure of files on: " + folder)

		opts.Alphabet = randomfiles.RunesEasy
		opts.Source = crand.Reader
		opts.FileSize = 4096
		opts.FanoutDepth = 4
		opts.FanoutDirs = 5
		opts.FanoutFiles = 10
		opts.RandomSize = true

		rand.Seed(time.Now().UnixNano())

		if err := os.MkdirAll(folder, 0755); err != nil {
			fmt.Println(err)
		}

		err := randomfiles.WriteRandomFiles(folder, 1, &opts)
		if err != nil {
			fmt.Println(err)
		}

		if err := createRandomFlagFile(folder); err != nil {
			fmt.Println(err)
		}
	}
}
