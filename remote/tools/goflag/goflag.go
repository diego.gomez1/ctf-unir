package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
	"os/user"
	"log"
	"runtime"
	"syscall"
)

func runAsUserName(desiredUserName string) bool {
	runtime.LockOSThread()
	// We do not have logging set up yet. We just panic() on error.
	fmt.Println("Trying to change to:", desiredUserName)

	if desiredUserName == "" {
		return false
	}

	binary, err := exec.LookPath("su")
	if err != nil {
		log.Fatalln("Unable to find 'su':", err)
	}

	var oUid = flag.Int("uid", 0, "Run with User ID")
	var oGid = flag.Int("gid", 0, "Run with Group ID")
	flag.Parse()

	// Get UID/GUID from args
	var uid = *oUid
	var gid = *oGid

	// Run whoami
	out, err := exec.Command("whoami").Output()
	if err != nil {
		log.Fatal(err)
		return true
	}

	// Output whoami
	log.Println("Original UID/GID whoami:", string(out))
	log.Println("Setting UID/GUID")

	// Change privileges

	err = syscall.Setgid(gid)

	err = syscall.Setuid(uid)
	if err != nil {
		log.Println("Cannot setuid")
		log.Fatal(err)
		return false
	}

	// Execute whoami again
	out, err = exec.Command("whoami").Output()
	if err != nil {
		log.Fatal(err)
		return false
	}
	log.Println("Changed UID/GID whoami:", string(out))

	cmd := exec.Command("whoami")
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()

	args := []string{"su", "-", desiredUserName, "-c", "/bin/sh", "--"}
	err = syscall.Exec(binary, args, os.Environ())

	return true
}

func main() {

	if len(os.Args) == 1 {
		fmt.Println("A flag is required")
		fmt.Println("Usage:", os.Args[0], "PATTERN", "FILE")
		return
	} else if len(os.Args) == 2 {
		flag := os.Args[1]
		fmt.Println("Input flag is: " + flag)
		activeUser, _ := user.Current()
		username := activeUser.Username

		if username == "root" && flag == "test" {
			runAsUserName("user02")
		} else if username == "diego" && flag == "test" {
			runAsUserName("user02")
		} else if username == "user01" && flag == "a1" {
			fmt.Println("User01 el flag es válido")
			runAsUserName("user02")
		} else {
			fmt.Println("Flag inválido")
		}
	}
}
