# List of tasks

## User01

- Password: linux
- Tasks: Open terminal

## User02

- Commands: cd
- Password: LinusTorvalds
- Tasks: Navigate on directories
- Flag: ideo{this_is_my_special_dir}

## User03

- Commands: cat
- Password: Kernel
- Tasks: Navigate through directories and cat content.
- Flag: ideo{encontre_lo_que_buscaba_en_el_archivo}

## User04

- Command: man
- Password: GNULinux
- Tasks: 
    - Access to man.

## User05

- Comand: more/less
- Password: Debian
- Tasks: Find text in a long file.

## User06

- Command: strings
- Password: 0100110101
- Tasks: Find text in binary file (pdf).
- Flag: ideo{message_on_pdf_but_hidden_wow}

## User07

- Command: grep
- Password: lots&lotsofFiles
- Tasks: Find text in folder with a lot of files
- Flag: ideo{here_its_with_a_Lot_of_files}

## User08

- Command: find
- Password: computersCanSearch
- Tasks: Estructura con muchos archivos usar find
    para localizar el flag.

## User08

- Command: cd && cat && grep
- Password: notOnlyText!
- Tasks: Directorio con varios archivos codificados
    en hexadecimal. La pista se incluye en el archivo
    `index.js` que tiene un algoritmo básico que hay
    que entender.


