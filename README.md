# CTF-UNIR

Entorno de juego de Capturar la Bandera para proyecto de intervención que fomente la didáctica de la seguridad en 2º de bachillerato.

## Dependencias

Este proyecto depende de los dos proyectos siguientes

- [Wetty = Web + TTY](https://github.com/butlerx/wetty)
- [RootTheBox](https://github.com/moloch--/RootTheBox)

Ambos proyectos se han incluido como módulos de este proyecto. Para descargarlos ejecuta el siguiente comando:

```bash
git submodule init
git submodule update
```

## Despliegue de proyecto

Todo el sistema está coordinado por medio de [`docker-compose`](https://docs.docker.com/compose/install/). Es necesario tenerlo instalado antes de continuar.

Tener en cuenta que el entorno se diseñó para ser ejecutado dentro de la red de la escuela, por eso no se están teniendo en cuenta temas como 
una protección adicional o uso de certificados.

Para lanzar el proyecto basta con ejecutar el siguiente comando.

```bash
docker compose up
```

## Enlaces

Con el anterior comando se lanzan los siguientes servicios:

- http://<host>/wetty - Consola ssh en web para acceder al servidor de Linux para las pruebas de capturar la bandera.
- http://<host>:8888 - Aplicación de Root the Box.

## Configuración de Root The Box

El entorno de Root The Box está completamente limpio, sin ninguna actividad, por lo que para lanzar los ejercicios habrá que:

- Crear una cuenta de administrador.
- Acceder a la sección "Backup/Restore" > "Import XML"
- Cargar desde el formulario el archivo `ctfGameBackup.xml`

Una vez realizados estos pasos puede ser necesario cambiar algunos datos de algunas actividades, como el acceso al servidor de Wetty.

## Remote

El directorio remote incluye la máquina de linux a la que se conectan los alumnos, dentro hay un archivo README donde se listan los diferentes
usuarios y los desafíos que se llevan a cabo en cada nivel.